ImagesLanguages = new FS.Collection("images-languages",{
    stores:[
        new FS.Store.GridFS("thumbnail", {
            transformWrite: function(fileObj, readStream, writeStream) {
                gm(readStream, fileObj.name()).resize('32', '32', '!').stream().pipe(writeStream);
            }
        })
    ],
    filter:{
        allow:{
            contentType: ['image/*']
        }
    }
});

ImagesLanguages.allow({
    insert: function (userId) {
        return (userId ? true : false);
    },
    remove: function (userId) {
        return (userId ? true : false);
    },
    download: function () {
        return true;
    },
    update: function (userId) {
        return (userId ? true : false);
    }
});

