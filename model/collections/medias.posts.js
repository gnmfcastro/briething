MediasPosts = new FS.Collection("medias-posts",{
    stores:[
        new FS.Store.GridFS("media"),
        new FS.Store.GridFS("mediaThumb", {
            transformWrite: function(fileObj, readStream, writeStream) {
                gm(readStream, fileObj.name()).resize('32', '32', '!').stream().pipe(writeStream);
            }
        }),
        new FS.Store.GridFS("mediaSmall", {
            transformWrite: function(fileObj, readStream, writeStream) {
                gm(readStream, fileObj.name()).resize('128', '128', '!').stream().pipe(writeStream);
            }
        })
    ],
    filter:{
        allow:{
            contentType: ['image/*']
        }
    }
});

MediasPosts.allow({
    insert: function (userId) {
        return (userId ? true : false);
    },
    remove: function (userId) {
        return (userId ? true : false);
    },
    download: function () {
        return true;
    },
    update: function (userId) {
        return (userId ? true : false);
    }
});