angular.module('briething',[
    'ngMaterial',
    'ngAnimate',
    'ngMessages',
    'ngFileUpload',
    'angular-meteor',
    'ui.router'
]);

var theme = function ($mdIconProvider,$mdThemingProvider) {

  $mdIconProvider
    .iconSet("social","/packages/planettraining_material-design-icons/bower_components/material-design-icons/sprites/svg-sprite/svg-sprite-social.svg")
    .iconSet("action","/packages/planettraining_material-design-icons/bower_components/material-design-icons/sprites/svg-sprite/svg-sprite-action.svg")
    .iconSet("communication","/packages/planettraining_material-design-icons/bower_components/material-design-icons/sprites/svg-sprite/svg-sprite-communication.svg")
    .iconSet("content","/packages/planettraining_material-design-icons/bower_components/material-design-icons/sprites/svg-sprite/svg-sprite-content.svg")
    .iconSet("toggle","/packages/planettraining_material-design-icons/bower_components/material-design-icons/sprites/svg-sprite/svg-sprite-toggle.svg")
    .iconSet("navigation","/packages/planettraining_material-design-icons/bower_components/material-design-icons/sprites/svg-sprite/svg-sprite-navigation.svg")
      .iconSet("av","/packages/planettraining_material-design-icons/bower_components/material-design-icons/sprites/svg-sprite/svg-sprite-av.svg")
    .iconSet("image","/packages/planettraining_material-design-icons/bower_components/material-design-icons/sprites/svg-sprite/svg-sprite-image.svg");

    $mdThemingProvider.theme('docsDark').primaryPalette('red').dark();

    var customPrimary = {
        '50': '#ffffff',
        '100': '#ffffff',
        '200': '#ffffff',
        '300': '#ffffff',
        '400': '#ffffff',
        '500': '#FFFFFF',
        '600': '#f2f2f2',
        '700': '#e6e6e6',
        '800': '#d9d9d9',
        '900': '#cccccc',
        'A100': '#ffffff',
        'A200': '#ffffff',
        'A400': '#ffffff',
        'A700': '#bfbfbf'
    };
    $mdThemingProvider
        .definePalette('customPrimary',
        customPrimary);

    var customAccent = {
        '50': '#f7aea4',
        '100': '#f5998d',
        '200': '#f38575',
        '300': '#f1705e',
        '400': '#ef5b46',
        '500': '#ED462F',
        '600': '#eb3118',
        '700': '#d62a13',
        '800': '#bf2611',
        '900': '#a7210e',
        'A100': '#f9c3bc',
        'A200': '#fbd8d3',
        'A400': '#fdedeb',
        'A700': '#901c0c'
    };
    $mdThemingProvider
        .definePalette('customAccent',
        customAccent);

    var customWarn = {
        '50': '#404040',
        '100': '#333333',
        '200': '#262626',
        '300': '#1a1a1a',
        '400': '#0d0d0d',
        '500': '#000000',
        '600': '#000000',
        '700': '#000000',
        '800': '#000000',
        '900': '#000000',
        'A100': '#4d4d4d',
        'A200': '#595959',
        'A400': '#666666',
        'A700': '#000000'
    };
    $mdThemingProvider
        .definePalette('customWarn',
        customWarn);

    var customBackground = {
        '50': '#ffffff',
        '100': '#ffffff',
        '200': '#fafafa',
        '300': '#ededed',
        '400': '#e1e1e1',
        '500': '#D4D4D4',
        '600': '#c7c7c7',
        '700': '#bababa',
        '800': '#aeaeae',
        '900': '#a1a1a1',
        'A100': '#ffffff',
        'A200': '#ffffff',
        'A400': '#ffffff',
        'A700': '#949494'
    };
    $mdThemingProvider
        .definePalette('customBackground',
        customBackground);

    $mdThemingProvider.theme('default')
        .primaryPalette('customPrimary')
        .accentPalette('customAccent')
        .warnPalette('customWarn')
        .backgroundPalette('customBackground')
};

angular.module('briething').config(theme);

function onReady() {
  angular.bootstrap(document, ['briething']);
}

if (Meteor.isCordova)
  angular.element(document).on("deviceready", onReady);
else
  angular.element(document).ready(onReady);
