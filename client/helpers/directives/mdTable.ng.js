angular.module('briething').directive('mdTable', function ($meteor) {
  return {
    restrict: 'E',
    scope: {
      headers: '=',
      content: '=',
      sortable: '=',
      filters: '=',
      customClass: '=customClass',
      thumbs:'=',
      count: '=',
      actions: '='
    },
    controller: function ($scope,$filter,$mdBottomSheet) {
      var orderBy = $filter('orderBy');

      $scope.tablePage = 0;
      $scope.nbOfPages = function () {

        return ($scope.content.length > $scope.count) ? Math.ceil($scope.content.length / $scope.count) : 1;
      };
      $scope.handleSort = function (field) {

          if ($scope.sortable.indexOf(field) > -1) { return true; } else { return false; }
      };

      $scope.order = function(predicate, reverse) {
          $scope.content = orderBy($scope.content, predicate, reverse);
          $scope.predicate = predicate;
      };

      $scope.order($scope.sortable[0],false);
      $scope.getNumber = function (num) {
      			    return new Array(num);
      };
      $scope.goToPage = function (page) {
        $scope.tablePage = page;
      };

      $scope.showActionList = function($event,item){
        $mdBottomSheet.show({
          templateUrl: 'client/helpers/views/actionListTemplate.ng.html',
          controller: 'ActionListCtrl',
          targetEvent: $event,
          locals: {
            item: item,
            actions: $scope.actions
          }
        }).then(function(clickedItem){
          $scope.expr({action: clickedItem.action},{item: clickedItem.item});
        });
      };

      $scope.expr = function(action,locals) {

        for(var i=0;i<$scope.actions.length;i++){
          if(action.action === $scope.actions[i].name){
            $scope.actions[i].click(locals.item);
            return;
          }
        }
      };

      $scope.convertFormatDate= function(date){
        if(date) {
          return $filter('date')(date, "MM/dd/yyyy");
        }else{
          return ""
        }
      };

      $scope.getThumbUrl = function(id){
        var image = $meteor.object(ImagesLanguages,{_id:id},false);

        if(image.url) {
          return image.url({store: 'thumbnail'});
        }else{
          return "";
        }
      };
    },
    templateUrl: 'client/helpers/views/mdTableTemplate.ng.html'
  };
});

angular.module('briething').directive('mdColresize', function ($timeout) {
  var result = {
    restrict: 'E',
    link: function (scope, element) {
      scope.$evalAsync(function () {
        $timeout(function(){ $(element).colResizable({
          liveDrag: true,
          fixed: true

        });},100);
      });
    }
  };

  return result;
});

angular.module('briething').filter('startFrom',function (){
  return function (input,start) {
    start = +start;
    return input.slice(start);
  }
});
