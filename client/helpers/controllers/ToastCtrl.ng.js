angular.module('briething').controller("ToastCtrl",function($rootScope,$scope,$meteor,$state,$mdToast,message,type){

  $scope.message = message;

  $scope.type = type;

  $scope.closeToast = function(){
    $mdToast.hide();
  };

});
