/**
 * Created by gcastro on 07/08/2015.
 */
angular.module('briething').controller("ActionListCtrl",function($rootScope,$scope,item,actions,$mdBottomSheet){

    $scope.item = item;
    $scope.actions = actions;

    $scope.clicked = function(action,item){
        $mdBottomSheet.hide({action: action,item:item});
    }

});
