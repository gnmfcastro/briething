angular.module('briething').controller("HeaderBlogCtrl",function($scope,$meteor){

    $scope.filters = [
        {
            item: "All categories",
            value: true
        }
    ];

    $scope.$meteorSubscribe("categories").then(function() {
        $scope.categories = $scope.$meteorCollection(function () {
            return Categories.find({});
        }, false);
    });

});
