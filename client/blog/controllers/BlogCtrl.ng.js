angular.module('briething').controller("BlogCtrl",function($rootScope,$scope,$meteor,$state,$window,$sce){

  $scope.state = $state;

  $rootScope.isPublic = true;

  $scope.posts = $meteor.collection(Posts, false);

    $scope.postsToShow = [];

    $scope.currentPost;

  $scope.$meteorSubscribe('languages').then(function(){
      $scope.languages = $scope.$meteorCollection(Languages,false);

      // SELECTING USER LANGUAGE
      $scope.userLanguage = $window.navigator.language || $window.navigator.userLanguage;
      $scope.userLanguage = $scope.userLanguage.toLowerCase();
      $scope.selectedLanguage = $scope.$meteorCollection(function(){
        return Languages.find( { shortNames: $scope.userLanguage } );
      },false);

      if($scope.selectedLanguage.length == 0){
        $scope.selectedLanguage = $scope.$meteorCollection(function(){
           return Languages.find( { isDefault: true } );
        },false);

        $scope.selectedLanguage = $scope.selectedLanguage[0];

      }else {
        $scope.selectedLanguage = $scope.selectedLanguage[0];
      }

      $scope.$meteorSubscribe("images");

      $scope.$meteorSubscribe("categories").then(function() {
          $scope.categories = $scope.$meteorCollection(function () {
              return Categories.find({});
          }, false);
      });

      $scope.$meteorSubscribe('postsLanguages').then(function(){

          $scope.postsLanguages = $scope.$meteorCollection(function(){
              return PostsLanguages.find({});
          },false);

          $scope.$meteorSubscribe('posts').then(function(){

              $scope.postsLanguages.forEach(function(postLanguage){

                  var result = {
                      _id: postLanguage._id,
                      medias: postLanguage.medias,
                      dateInserted: postLanguage.dateInserted,
                      post: $scope.getPost(postLanguage)
                  };

                  if(result.post != ""){
                      $scope.postsToShow.push(result);
                  }
              });

              $scope.currentPost = $scope.postsToShow[0];
          });
      });
  });

  $scope.getLanguageImgUrl = function(id){
    var img = $scope.$meteorObject(ImagesLanguages,{_id: id},false);

    if(img.url) {
      return img.url({store: 'thumbnail'});
    }else{
      return "";
    }
  };

  $scope.openLanguageMenu = function($mdOpenMenu, ev) {
    $mdOpenMenu(ev);
  };

  $scope.changeLanguage = function(id){

    $scope.selectedLanguage = $scope.$meteorCollection(function(){
      return Languages.find( { _id: id } );
    },false);

    $scope.selectedLanguage = $scope.selectedLanguage[0];

      if($scope.postsToShow){
          $scope.postsToShow = [];
      }

      $scope.postsLanguages.forEach(function(postLanguage){

          var result = {
              _id: postLanguage._id,
              medias: postLanguage.medias,
              dateInserted: postLanguage.dateInserted,
              post: $scope.getPost(postLanguage)
          };

          if(result.post != ""){
              $scope.postsToShow.push(result);
          }
      });

      if($scope.currentPost){

          var oldCurrentPost = $scope.currentPost;

          $scope.postsToShow.forEach(function(item){
              if($scope.currentPost._id == item._id){
                  $scope.currentPost = item;
                  return;
              }
          });

          if(oldCurrentPost == $scope.currentPost){
              $scope.currentPost = $scope.postsToShow[0];
          }

      }else{
          $scope.currentPost = $scope.postsToShow[0];
      }
  };

    $scope.selectPost = function(id){

        $scope.postsToShow.forEach(function(item){

            if(id == item._id){
                $scope.currentPost = item;
                return;
            }

        });
    };

    $scope.buildPostContent = function(content,medias){

        var regexImg = /\{i:([^)]+?)\}/g;
        var regexVideo = /\{v:([^)]+?)\}/g;
        var resultImg, resultVideo;
        var imgs = [];
        var videos = [];

        while((resultImg = regexImg.exec(content))){
            imgs.push(resultImg);
        }

        while((resultVideo = regexVideo.exec(content))){
            videos.push(resultVideo);
        }

        if(imgs.length > 0){
            for(var i=0;i<imgs.length;i++){

                var img = imgs[i];

                for(var j=0; j<medias.length;j++){
                    if(img[1] == medias[j].name){

                        var replaceStr = '<br/><div layout-padding layout="row" layout-align="center center"><img ng-src="'+medias[j].url+'" src="'+medias[j].url+'" alt="'+img[1]+'" /></div><br/>';

                        content = content.replace(img[0],replaceStr);
                    }
                }
            }
        }

        if(videos.length > 0){
            for(var i=0; i<videos.length;i++){

                var video = videos[i];

                console.log(videos);

                for(var j=0; j<medias.length; j++){
                    if(video[1] == medias[j].name){

                        var replaceStr;

                        if(medias[j].from == 'YOUTUBE'){

                            var regexVideoId = /v=([^)]+)/;

                            var videoId = regexVideoId.exec(medias[j].url);

                            replaceStr = '<div layout-padding layout="row" layout-align="center center"><iframe width="560" height="315" src="http://www.youtube.com/embed/'+videoId[1]+'?autoplay=0" frameborder="0" allowfullscreen></iframe></div>';

                        }else if(medias[j].from == 'VIMEO'){

                            var regexVideoId = /vimeo.com\/([^)]+)/;

                            var videoId = regexVideoId.exec(medias[j].url);

                            replaceStr = '<div layout-padding layout="row" layout-align="center center"><iframe src="https://player.vimeo.com/video/'+videoId[1]+'" width="560" height="315" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe></div>';

                        }else{
                            replaceStr = '';
                        }

                        content = content.replace(video[0],replaceStr);
                    }
                }
            }
        }

        return $sce.trustAsHtml(content);

    };

    $scope.getPost = function(postLanguage){
        var out;
        if (this.posts.length > 0) {
            for (var i = 0; i < postLanguage.posts.length; i++) {
                out = $scope.$meteorCollection(function () {
                    return Posts.find({_id: postLanguage.posts[i], language: $scope.selectedLanguage._id});
                }, false);

                if (out.length == 1) {
                    out = out[0];
                    break;
                }
            }

            out.content = $scope.buildPostContent(out.content,postLanguage.medias);

            return out;
        } else {
            return "";
        }
    };

});
