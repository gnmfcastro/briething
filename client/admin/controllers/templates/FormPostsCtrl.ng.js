angular.module('briething').controller("FormPostsCtrl",function($rootScope,$scope,$meteor,$state,$mdDialog, $mdSidenav, $mdUtil,  languagesAdded, postLanguage, Utils) {

    /*********************************************************************************
     * LANGUAGES - VARS
     *********************************************************************************/

    $scope.readonly             = false;
    $scope.languagesAdded       = languagesAdded; //VECTOR OF LANGUAGES ADDED
    $scope.selectedLanguage     = ""; // INICIALIZATION OF SELECTED LANGUAGE
    $scope.categoriesAdded      = postLanguage.categories ? postLanguage.categories : [];

    console.log($scope.categoriesAdded);


    /*********************************************************************************
     * MD-ALTOCOMPLETE VARS
     *********************************************************************************/

    $scope.filterSelected       = true;
    $scope.query                = null;
    $scope.searchCategoriesTxt  = null;

    /*********************************************************************************
     * MEDIA FORMS VARS INI
     *********************************************************************************/

    $scope.showForm             = false;
    $scope.mediaUrlTyped        = "";
    $scope.showAddButton        = true;
    $scope.selectedIndex        = 0; // DEFAULT TAB SELECTED AT BEGINING 0 is PHOTOS and 1 is VIDEOS

    /*********************************************************************************
     * METEOR VARS INI
     *********************************************************************************/

    $scope.medias               = $meteor.collectionFS(MediasPosts, false, MediasPosts).subscribe('medias');
    $scope.postLanguage         = postLanguage;
    $scope.languages            = "";

    /*********************************************************************************
     * VARS TO BE FILLED AND SAVED TO THE DB
     *********************************************************************************/

    $scope.media = {
        url: "",
        urlSmall: "",
        urlThumb: "",
        name: "",
        mediaId: "",
        type: "",
        from: ""
    };

    $scope.post = {
        title: "",
        subtitle: "",
        content: "",
        footer: "",
        publisher: "",
        dateInserted: "",
        language: ""
    };


    /*********************************************************************************
     * METEOR SUBSCRIPTIONS
     *********************************************************************************/

    $scope.$meteorSubscribe( "languages" ).then(function() {

        $scope.languages = $scope.$meteorCollection(Languages,false);

        $scope.language = $scope.$meteorCollection(function(){
            return Languages.find({isDefault:true});
        },false);

        $scope.selectedLanguage = $scope.language[0]._id;

        //CHECK IF THERE IS LOADED POST LANGUAGE OR NOT
        if($scope.postLanguage.posts.length <= 0) {

            //JUST ADDING THE DEFAULT LANGUAGE AND SETTING THAT IS THE SELECTED ONE
            $scope.languagesAdded = $scope.language;

            //SETTING THE FIRST POST LANGUAGE WHEN IS A NEW POST
            $scope.post.language = $scope.selectedLanguage;

            $scope.postLanguage.posts.push($scope.post);

        }else if($scope.postLanguage.posts.length > 0){

            $scope.loadPostsAdded();
        }

        $scope.$meteorSubscribe("categories").then(function(){
            $scope.categories = $scope.$meteorCollection(function(){
               return Categories.find({});
            },false);
        });

    });

    $scope.$meteorSubscribe("images");


    /*********************************************************************************
     * MAIN FORM EVENTS
     *********************************************************************************/

    $scope.cancel = function(){
        $mdDialog.cancel();
    };

    $scope.openMedias = function(navID){
        $mdSidenav(navID)
            .toggle()
            .then(function () {

            });
    };

    $scope.savePost = function(event){

        if(!$scope.postLanguage._id) {

            $scope.postLanguage.dateInserted = Date.now();
            $scope.postLanguage.categories = $scope.categoriesAdded;

            $meteor.call('post_post', $scope.postLanguage)
                .then(function () {
                    $mdDialog.hide();
                    $rootScope.throwToast("Post Added", "success");
                },
                function (err) {
                    $rootScope.throwToast(err.message, "error");
                }
            );
        }else{

            $scope.postLanguage.categories = $scope.categoriesAdded;

            $meteor.call('post_update', $scope.postLanguage)
                .then(function () {
                    $mdDialog.hide("success");
                    $rootScope.throwToast("Post Updated", "success");
                },
                function (err) {
                    $rootScope.throwToast(err.message, "error");
                }
            );
        }
    };

    $scope.selectLanguage = function(id){

        var isNew = true;

        if(!$scope.post.language || $scope.post.language === ""){
            $scope.post.language = $scope.selectedLanguage;
        }

        if($scope.postLanguage && $scope.postLanguage.posts.length > 0){

            for(var i=0; i < $scope.postLanguage.posts.length; i++){

                var post = $scope.postLanguage.posts[i];

                if(post.language === id){
                    $scope.post = post;
                    isNew = false;
                    break;
                }
            }
        }

        if(isNew){

            $scope.post = {
                title: "",
                subtitle: "",
                content: "",
                footer: "",
                publisher: "",
                dateInserted: "",
                language: id
            };

            $scope.postLanguage.posts.push($scope.post);
        }

        $scope.selectedLanguage = id;
    };

    $scope.getThumbUrl = function(id){
        var img = $scope.$meteorObject(ImagesLanguages,{_id: id},false);

        if(img.url) {
            return img.url({store: 'thumbnail'});
        }else{
            return "";
        }
    };

    $scope.languageSearch = function(query){

        if($scope.languagesAdded.length > 0 && $scope.languagesAdded.length !== $scope.languages.length) {
            var lowercaseQuery = angular.lowercase(query);
            var languagesAddedIds = [];

            for (var i = 0; i < $scope.languagesAdded.length; i++) {
                languagesAddedIds.push($scope.languagesAdded[i]._id);
            }

            var results = query ? $scope.$meteorCollection(function () {
                return Languages.find({
                    $and: [
                        {'name': {'$regex': '.*' + lowercaseQuery + '.*', '$options': 'i'}},
                        {'_id': {'$nin': languagesAddedIds}}
                    ]
                });
            }, false) : [];

            return results;
        }else{
            return [];
        }
    };

    $scope.categoriesSearch = function(query){

        if($scope.categoriesAdded.length !== $scope.categories.length) {
            var lowercaseQuery = angular.lowercase(query);
            var categoriesAddedIds = [];

            for (var i = 0; i < $scope.categoriesAdded.length; i++) {
                categoriesAddedIds.push($scope.categoriesAdded[i]._id);
            }

            var results = query ? $scope.$meteorCollection(function () {
                return Categories.find({
                    $and: [
                        {'name': {'$regex': '.*' + lowercaseQuery + '.*', '$options': 'i'}},
                        {'_id': {'$nin': categoriesAddedIds}}
                    ]
                });
            }, false) : [];

            console.log(results);

            return results;



        }else{
            return [];
        }
    };

    $scope.loadPostsAdded = function(){

        var posts =  [];
        var languagesAdded = [];

        for(var i=0; i<$scope.postLanguage.posts.length; i++){

            var post = $scope.$meteorObject(Posts, {_id: $scope.postLanguage.posts[i]}, false).getRawObject();

            var language = $scope.$meteorObject(Languages, {_id: post.language},false).getRawObject();

            languagesAdded.push(language);

            posts.push(post);
        }

        $scope.languagesAdded = languagesAdded;
        $scope.postLanguage.posts = posts;

        $scope.selectLanguage($scope.selectedLanguage);
    };

    //WATCHERS MAIN FORM

    $scope.$watchCollection('languagesAdded', function(newVal, oldVal) {
        // A chip has been removed if oldVal is greater in size than newVal
        if (angular.isArray(oldVal) && oldVal.length > newVal.length) {
            // Find the item(s) in oldVal that does
            // not exist anymore in newVal.
            var diff = oldVal.filter(function(a) {
                return newVal.filter(function(b) {
                        return a === b;
                    }).length == 0
            });

            if (diff.length == 1 && diff[0].isDefault) {
                $scope.languagesAdded = oldVal;
            }
        }
    });

    /*********************************************************************************
     * EVENTS MEDIA SIDENAV
     *********************************************************************************/

    $scope.addImages = function(files){
        if(files.length > 0){
            var image = files[0];

            if($scope.media.mediaId !== "") {
                $scope.medias.remove({_id:$scope.media.mediaId});
            }

            $scope.medias.save(image).then(function (result) {
                var uploadedImage = result[0]._id;
                $scope.media.type = "PHOTO";
                $scope.media.mediaId = uploadedImage._id;
            });
        }
    };

    $scope.mediaUrl = function(url){

        Utils.isImage(url).then(function(result) {

            if(!result){
                if(url.indexOf("youtube.com/watch?v") > -1){
                    $scope.media.url = url;
                    $scope.media.from = "YOUTUBE";
                    $scope.media.type = "VIDEO";
                }else if(url.indexOf("vimeo.com/") > -1){
                    $scope.media.url = url;
                    $scope.media.from = "VIMEO";
                    $scope.media.type = "VIDEO";
                }
            }
        });
    };

    $scope.showImageSmall = function(id){
        var image = $meteor.object(MediasPosts,{_id:id},false);

        if(image.url){
            $scope.media.url = image.url();
            $scope.media.urlSmall = image.url({store: 'mediaSmall'});
            $scope.media.urlThumb = image.url({store: 'mediaThumb'});
            return image.url({store: 'mediaSmall'});
        }else{
            return "";
        }
    };

    $scope.removeMedia = function(media){

        for(var i=0; i<$scope.postLanguage.medias.length; i++){
            var medias = $scope.postLanguage.medias[i];
            if(media.type == medias.type && medias.name == media.name){
                $scope.postLanguage.medias.splice(i,1);
                $rootScope.throwToast("Media removed from post", "success");
                break;
            }
        }

        if(media.type == "PHOTO"){
            $scope.medias.remove({_id:media.mediaId});
        }
    };

    $scope.showAddForm = function(event){
        $scope.showForm = true;
        $scope.showAddButton = false;
    };

    $scope.hideAddForm = function(event){
        $scope.showForm = false;
        $scope.showAddButton = true;
    };

    $scope.saveMedia = function(media){
        media = $scope.isValidMedia(media);

        if(media){
            $scope.postLanguage.medias.push(media);
            $scope.hideAddForm(undefined);

            $rootScope.throwToast("Media added to post", "success");

            $scope.media = {
                url: "",
                urlSmall: "",
                urlThumb: "",
                name: "",
                mediaId: "",
                type: "",
                from: ""
            };

        }else{
            $rootScope.throwToast("Error saving media", "error");
        }
    };

    $scope.isValidMedia = function(media){
      if(media.name && media.name != ""){

          for(var i=0; i<$scope.postLanguage.medias.length; i++){
              var medias = $scope.postLanguage.medias[i];
              if(media.type == medias.type && medias.name == media.name){
                  return false;
              }
          }

          if(media.type == "PHOTO"){
              media.from = "LOCAL";

              if(media.mediaId && media.mediaId != "") {
                  return media;
              }else{
                  return false;
              }

          }else if(media.type == "VIDEO"){
              if(media.mediaId && media.mediaId != ""){
                  $scope.medias.remove({_id:media.mediaId});
                  media.mediaId = "";
                  media.urlSmall = "";
                  media.urlThumb = "";
              }

              if(media.url != "" && media.from != ""){
                  return media;
              }else{
                  return false;
              }
          }

      }else{
          return false;
      }
    };

    $scope.next = function() {
        $scope.selectedIndex = Math.min($scope.data.selectedIndex + 1, 2) ;
    };

    $scope.previous = function() {
        $scope.selectedIndex = Math.max($scope.data.selectedIndex - 1, 0);
    };

});
