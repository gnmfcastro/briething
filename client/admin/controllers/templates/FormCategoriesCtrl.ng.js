angular.module('briething').controller("FormCategoriesCtrl",function($rootScope,$scope,$meteor,$state,$mdDialog){

    $scope.readonly = false;

    $scope.saveCategory = function(event){
        if($scope.category._id){
            //UPDATE
            $meteor.call('category_update',$scope.category.getRawObject())
                .then(function(){
                    $mdDialog.hide();
                    $rootScope.throwToast("Category updated","success");
                },
                function(err){
                    $rootScope.throwToast(err.message, "error");
                }
            );

        }else{
            //CREATE
            $meteor.call('category_post', $scope.category.name, $scope.category.color)
                .then(function(){
                    $mdDialog.hide();
                    $rootScope.throwToast("Category Added","success");
                },
                function(err){
                    $rootScope.throwToast(err.message, "error");
                }
            );
        }
    };

    $scope.cancel = function() {
        $mdDialog.cancel();
    };

});
