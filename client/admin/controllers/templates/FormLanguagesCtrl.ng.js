angular.module('briething').controller("FormLanguagesCtrl",function($rootScope,$scope,$meteor,$state,$mdDialog){

    $scope.readonly = false;

    $scope.image = {};

    $scope.addImages = function(files){
        if(files.length > 0){
            $scope.image = files[0];

            $scope.images.save($scope.image).then(function(result) {
                $scope.uploadedImage = result[0]._id;

                $scope.language.image = $scope.uploadedImage;
            });
        }
    };

    $scope.cancel = function() {
        if(!$scope.language._id
            && $scope.language.image !== undefined
            && $scope.language.image !== '') {

            $scope.images.remove($scope.language.image);
        }

        $mdDialog.cancel();
    };

    $scope.deleteImage = function(image){

        $scope.language.image = "";
        $scope.images.remove(image);

    };

  $scope.saveLanguage = function(){

      if($scope.language._id){
          //UPDATE
          if($scope.language.image !== "") {
              if($scope.language.image._id) {
                  $scope.language.image = $scope.language.image._id;
              }
          }

          $meteor.call('language_update',$scope.language.getRawObject())
              .then(function(){
                  $mdDialog.hide();
                  $rootScope.throwToast("Language updated","success");
              },
              function(err){
                  $rootScope.throwToast(err.message, "error");
              }
          );

      }else{
          //CREATE
          $meteor.call('language_post', $scope.language.name,$scope.language.shortNames,$scope.language.image._id, $scope.language.isDefault)
              .then(function(){
                  $mdDialog.hide();
                  $rootScope.throwToast("Language Added","success");
              },
              function(err){
                  $rootScope.throwToast(err.message, "error");
              }
          );
      }
  };
});
