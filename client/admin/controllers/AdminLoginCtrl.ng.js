angular.module('briething').controller("AdminLoginCtrl",function($rootScope,$scope,$meteor,$state){

  if($rootScope.currentUser !== null || $rootScope.currentUser !== undefined){
    $state.go('admin');
  }

  $scope.login = function(){
    $meteor.loginWithPassword($scope.user.username, $scope.user.password).then(function(){
      $state.go('admin');
    }, function(err){
      $rootScope.throwToast("Login error!","error");
    });
  }
});
