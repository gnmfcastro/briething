angular.module('briething').controller("AdminLangsCtrl",function($rootScope,$scope,$meteor,$state,$mdToast,$mdDialog){

    $scope.toggleSearch = false;

    $scope.headers = [
        {
            name:'',
            field:'thumb'
        },
        {
          name: 'Name',
          field: 'name'
        },
        {
            name:'Short Names',
            field: 'shortNames'
        },
        {
            name:'Default Language',
            field: 'isDefault'
        }
    ];

    $scope.$meteorSubscribe("images").then(function(){
       $scope.images = $meteor.collectionFS(ImagesLanguages, false, ImagesLanguages);
    });

    $scope.$meteorSubscribe("languages").then(function(){
        $scope.languages = $scope.$meteorCollection(Languages,false);

        $scope.content = $scope.languages;
    });

    $scope.content = [
        {
            thumb:'',
            name:'',
            shortNames:''
        }
    ];

  $scope.actions = [{
    name:'Edit',
    icon: 'content:ic_create_24px',
    click: function(item){

        $scope.language = $meteor.object(Languages,{_id:item._id},false);

        if($scope.language.image !== "") {
            $scope.language.image = $meteor.object(ImagesLanguages, {_id: $scope.language.image}, false);
        }

        $mdDialog.show({
            clickOutsideToClose: true,
            scope: $scope,
            preserveScope: true,
            controller: 'FormLanguagesCtrl',
            templateUrl: 'client/admin/views/templates/formLanguages.tmpl.ng.html',
            targetEvent: event
        });
    }
  },{
    name:'Delete',
    icon: 'action:ic_delete_24px',
    click: function(item){

        $scope.language = $meteor.object(Languages,{_id:item._id},false);

        var confirm = $mdDialog.confirm()
            .parent(angular.element(document.body))
            .title('Would you like to delete this language?')
            .content('All posts with this language will be deleted')
            .ariaLabel('Delete Language')
            .ok('Yes')
            .cancel('Cancel');

        $mdDialog.show(confirm).then(function() {

            $meteor.call('language_delete',$scope.language.getRawObject())
                .then(function(){
                    $rootScope.throwToast("Language deleted","success");
                },
                function(err){
                    $rootScope.throwToast(err.message,"error");
                }
            );

        });

    }
  }];

    $scope.images = $meteor.collectionFS(ImagesLanguages, false, ImagesLanguages).subscribe('images');
    $scope.sortable = ['name'];
    $scope.custom = {name: 'bold', shortNames:'grey'};
    $scope.thumbs = 'thumb';
    $scope.count = 3;
    $scope.readonly = false;


  //Functions and Events goes bellow

  $scope.showForm = function(event){

      $scope.language = {
          name: "",
          shortNames: [],
          image: "",
          isDefault: false
      };

      $mdDialog.show({
          clickOutsideToClose: true,
          scope: $scope,
          preserveScope: true,
          controller: 'FormLanguagesCtrl',
          templateUrl: 'client/admin/views/templates/formLanguages.tmpl.ng.html',
          targetEvent: event
      });

  };

});
