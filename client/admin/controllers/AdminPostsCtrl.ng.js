angular.module('briething').controller("AdminPostsCtrl",function($rootScope,$scope,$meteor,$state,$mdToast,$mdDialog){

  $scope.toggleSearch = false;
  $scope.headers = [
    {
      name:'',
      field:'thumb'
    },{
      name: 'Title',
      field: 'title'
    },{
      name: 'Subtitle',
      field: 'subtitle'
    },{
      name:'Publisher',
      field: 'publisher'
    },{
      name: 'Date',
      field: 'dateInserted'
    }
  ];

  $scope.$meteorSubscribe("posts").then(function(hPosts) {
        $scope.posts = $scope.$meteorCollection(Posts,false);

        $scope.$meteorSubscribe("postsLanguages").then(function(hPostsLangs){
          $scope.postsLanguages = $scope.$meteorCollection(PostsLanguages,false);
        });
  });

  $scope.content = [{
    thumb: "",
    title: "",
    subtitle: "",
    publisher: "",
    dateInserted: ""
  }];

  $scope.languagesAdded = [];

  $scope.actions = [{
    name:'Edit',
    icon: 'content:ic_create_24px',
    click: function(item){

      $scope.postLanguage = $scope.$meteorObject(PostsLanguages, {_id: item._id}, false).getRawObject();

      $scope.languagesAdded = [];

      $mdDialog.show({
        clickOutsideToClose: true,
        controller: 'FormPostsCtrl',
        locals: {
          postLanguage: $scope.postLanguage,
          languagesAdded: $scope.languagesAdded
        },
        templateUrl: 'client/admin/views/templates/formPosts.tmpl.ng.html',
        targetEvent: event
      }).then(function(){
        $scope.content = $scope.buildContent($scope.postsLanguages);
      });
    }
  },{
    name:'Delete',
    icon: 'action:ic_delete_24px',
    click: function(item){
      alert("Delete");
    }
  }];

  $scope.custom = {title: 'bold', subtitle:'grey',publisher: 'grey',dateInserted: 'bold' };
  $scope.sortable = ['title', 'dateInserted'];
  $scope.thumbs = 'thumb';
  $scope.count = 3;
  $scope.readonly = false;

  //Functions and Events goes bellow

  $scope.showForm = function(event){

    $scope.postLanguage = {
      posts: [],
      medias: []
    };

    $scope.languagesAdded = [];

    $mdDialog.show({
      clickOutsideToClose: true,
      controller: 'FormPostsCtrl',
      locals: {
        postLanguage: $scope.postLanguage,
        languagesAdded: $scope.languagesAdded
      },
      templateUrl: 'client/admin/views/templates/formPosts.tmpl.ng.html',
      targetEvent: event
    });

  };

  $scope.buildContent = function(postsLanguages){

    var result = [];

    for(var i=0; i < postsLanguages.length; i++){

      var post = $meteor.object(Posts, {_id: postsLanguages[i].posts[0]}, false);

      var item = {
        _id: postsLanguages[i]._id,
        thumb: "",
        title: post.title,
        subtitle: post.subtitle,
        publisher: $scope.getPublisher(post.publisher),
        dateInserted: postsLanguages[i].dateInserted
      };

      result.push(item);
    }

    return result;
  };

  $scope.$watch('postsLanguages', function(newVal, oldVal) {
    if(newVal) {
      $scope.content = $scope.buildContent(newVal);
    }
  },true);

  $scope.getPublisher = function(id){
    var user = $scope.$meteorObject(Meteor.users,{_id:id}, false);
    if(user.username) {
      return user.username;
    }else{
      return id;
    }
  };

});
