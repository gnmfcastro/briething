/**
 * Created by guilherme on 9/15/15.
 */
angular.module('briething').controller("AdminCatsCtrl",function($rootScope,$scope,$meteor,$state,$mdToast,$mdDialog) {

    $scope.toggleSearch = false;

    $scope.headers = [
        {
            name:'',
            field:'thumb'
        },
        {
            name: 'Name',
            field: 'name'
        },
        {
            name:'Color',
            field: 'color'
        }
    ];

    $scope.content = [
        {
            thumb:'',
            name:'',
            color:''
        }
    ];

    $scope.$meteorSubscribe('categories').then(function(){

        $scope.categories = $scope.$meteorCollection(function(){
            return Categories.find({});
        },false);

        $scope.content = $scope.categories;

    });

    $scope.sortable = ['name'];
    $scope.custom = {name: 'bold', shortNames:'grey'};
    $scope.thumbs = 'thumb';
    $scope.count = 3;
    $scope.readonly = false;

    $scope.actions = [{
        name:'Edit',
        icon: 'content:ic_create_24px',
        click: function(item){

            $scope.category = $meteor.object(Categories,{_id:item._id},false);

            $mdDialog.show({
                clickOutsideToClose: false,
                scope: $scope,
                preserveScope: true,
                controller: 'FormCategoriesCtrl',
                templateUrl: 'client/admin/views/templates/formCategories.tmpl.ng.html',
                targetEvent: event
            });
        }
    },{
        name:'Delete',
        icon: 'action:ic_delete_24px',
        click: function(item){

            $scope.category = $meteor.object(Categories,{_id:item._id},false);

            var confirm = $mdDialog.confirm()
                .parent(angular.element(document.body))
                .title('Would you like to delete this category?')
                .content('')
                .ariaLabel('Delete Category')
                .ok('Yes')
                .cancel('Cancel');

            $mdDialog.show(confirm).then(function() {

                $meteor.call('category_delete',$scope.category.getRawObject())
                    .then(function(){
                        $rootScope.throwToast("Category deleted","success");
                    },
                    function(err){
                        $rootScope.throwToast(err.message,"error");
                    }
                );

            });

        }
    }];


    $scope.showForm = function(event){

        $scope.category = {
            name: "",
            color: ""
        };

        $mdDialog.show({
            clickOutsideToClose: false,
            scope: $scope,
            preserveScope: true,
            controller: 'FormCategoriesCtrl',
            templateUrl: 'client/admin/views/templates/formCategories.tmpl.ng.html',
            targetEvent: event
        });

    };

});
