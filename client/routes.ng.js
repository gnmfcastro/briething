angular.module('briething').run(["$rootScope", "$state","$mdToast", "$meteor","$mdSidenav", function($rootScope, $state, $mdToast, $meteor, $mdSidenav) {

  $rootScope.toastPosition = {
    bottom: false,
    top: true,
    left: false,
    right: true
  };

  $rootScope.isPublic = false;

  $rootScope.getToastPosition = function() {
    return Object.keys($rootScope.toastPosition)
      .filter(function(pos) { return $rootScope.toastPosition[pos]; })
      .join(' ');
  };

  $rootScope.throwToast = function(message,type){
    $mdToast.show({
      controller: 'ToastCtrl',
      templateUrl: 'client/helpers/views/toastTemplate.ng.html',
      hideDelay: 6000,
      locals: {message: message, type: type},
      position: $rootScope.getToastPosition()
    });
  };

  $rootScope.logout = function(){
    $meteor.logout().then(function(){
      $state.go('login')
    });
  };

  $rootScope.toggleMenu = function(navID){
    $mdSidenav(navID)
        .toggle()
        .then(function () {

        });
  };

  $rootScope.$on("$stateChangeError", function(event, toState, toParams, fromState, fromParams, error) {
    // We can catch the error thrown when the $meteor.requireUser() promise is rejected
    // and redirect the user back to the login page
    if (error === "AUTH_REQUIRED") {
        // It is better to use $state instead of $location. See Issue #283.
        $state.go('login');
    }
  });
}]);

angular.module('briething').config(['$urlRouterProvider', '$stateProvider', '$locationProvider',
  function($urlRouterProvider, $stateProvider, $locationProvider){

    $locationProvider.html5Mode(true);

    $stateProvider
      .state('blog', {
        url: '/blog',
        views: {
          "header": {
            templateUrl:'client/blog/views/layout/header.ng.html',
            controller:'HeaderBlogCtrl'
          },
          "main": {
            templateUrl:'client/blog/views/blog.ng.html',
            controller:'BlogCtrl'
          },
          "footer":{
            templateUrl:'client/blog/views/layout/footer.ng.html',
            controller:'FooterBlogCtrl'
          }
        }
      })

//--------------------------- ADMIN ROUTES !!!!! -----------------------------//

      .state('admin', {
        url: '/blog/admin',
        views:{
            "header@":{
              templateUrl:'client/admin/views/layout/header.ng.html',
              controller:'HeaderAdminCtrl'
            },
            "main@": {
              templateUrl:'client/admin/views/admin.ng.html',
              controller:'AdminCtrl'
            },
            "footer@":{
              templateUrl:'client/admin/views/layout/footer.ng.html',
              controller:'FooterAdminCtrl'
            }
        },
        resolve: {
          "currentUser": ["$meteor", function($meteor){
            return $meteor.requireUser();
          }]
        }
      })
      .state('admin.posts', {
        url: '/posts',
        views:{
            "main@": {
              templateUrl:'client/admin/views/adminPosts.ng.html',
              controller:'AdminPostsCtrl'
            }
        },
        resolve: {
          "currentUser": ["$meteor", function($meteor){
            return $meteor.requireUser();
          }]
        }
      })
      .state('admin.languages',{
        url: '/langs',
        views:{
            "main@": {
              templateUrl: 'client/admin/views/adminLangs.ng.html',
              controller: 'AdminLangsCtrl'
            }
        },
        resolve: {
          "currentUser" : ["$meteor", function($meteor){
            return $meteor.requireUser();
          }]
        }
      })
        .state('admin.categories',{
          url: '/categories',
          views:{
            "main@":{
              templateUrl: 'client/admin/views/adminCats.ng.html',
              controller: 'AdminCatsCtrl'
            }
          },
          resolve: {
            "currentUser" : ["$meteor", function($meteor){
              return $meteor.requireUser();
            }]
          }
        })
      .state('login', {
        url: '/admin/login',
        views:{
            "header@":{
              template:""
            },
            "main@": {
              templateUrl:'client/admin/views/adminLogin.ng.html',
              controller:'AdminLoginCtrl'
            },
            "footer@":{
              template:""
            }
        },
        resolve: {
          "currentUser": ["$meteor", function($meteor){
            return $meteor.waitForUser();
          }]
        }
      });

      $urlRouterProvider.otherwise('/blog');
}]);
