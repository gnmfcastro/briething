class @PostLanguage
  @id
  @posts
  @medias
  @dateInserted
  @categories

  constructor:(@posts,@medias,@dateInserted,@categories) ->

  getId: () ->
    @id
  setId: (@id) ->
    @

  getPosts: () ->
    @posts
  setPosts: () ->
    @

  getMedias: () ->
    @medias
  setMedias: (@medias) ->
    @

  getDateInserted: () ->
    @dateInserted
  setDateInserted: (@dateInserted) ->
    @

  getCategories: () ->
    @categories
  setCategories: (@categories) ->
    @
