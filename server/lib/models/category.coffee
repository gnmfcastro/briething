class @Category
  @_id
  @name
  @color

  constructor: (@name, @color) ->

  getId: () ->
    @_id
  setId: (@_id)  ->
    @

  getName: () ->
    @name
  setName: (@name) ->
    @

  getColor: () ->
    @color
  setColor: (@color) ->
    @
