class @PostLanguageDAO

  constructor: () ->

  save: (newPostLanguage) ->
    if newPostLanguage.getId()
      return PostsLanguages.update(newPostLanguage.getId(),{
        $set:{
          posts: newPostLanguage.getPosts(),
          dateInserted: newPostLanguage.getDateInserted(),
          medias: newPostLanguage.getMedias(),
          categories: newPostLanguage.getCategories()
        }
      });
    else
      return PostsLanguages.insert({
        posts: newPostLanguage.getPosts(),
        dateInserted: newPostLanguage.getDateInserted(),
        medias: newPostLanguage.getMedias(),
        categories: newPostLanguage.getCategories()
      });
