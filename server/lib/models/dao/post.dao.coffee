class @PostDAO

  constructor:() ->

  save: (post) ->
    if post.getId()
      return Posts.update(post.getId(),{
        $set:{
          title: post.getTitle(),
          subtitle: post.getSubtitle(),
          content: post.getContent(),
          footer: post.getFooter(),
          publisher: post.getPublisher(),
          language: post.getLanguage()
        }
      });
    else
      return Posts.insert({
        title: post.getTitle(),
        subtitle: post.getSubtitle(),
        content: post.getContent(),
        footer: post.getFooter(),
        publisher: post.getPublisher(),
        language: post.getLanguage()
      });
