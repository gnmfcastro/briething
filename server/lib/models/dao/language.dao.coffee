class @LanguageDAO

  constructor:() ->

  exists:(newLanguage) ->
      language = Languages.find($or: [
        { name: newLanguage.getName() }
        { shorNames: $elemMatch: $in: newLanguage.getShortNames() }
      ]).fetch()

      if newLanguage.getId()
        if language.length == 1 and newLanguage.getId() == language[0]._id
          false
        else
          true
      else
        if language and language.length > 0
          true
        else
          false

  existsDefault:(newLanguage) ->
      language =  Languages.find({isDefault: true}).fetch();

      if newLanguage.getIsDefault() == true
        if newLanguage.getId()
          if language.length == 1 and language[0]._id == newLanguage.getId()
            false
          else
            if language.length <= 0
              false
            else
              true
        else
          if language and language.length > 0
            true
          else
            false
      else
        false

  save: (newLanguage) ->
    if newLanguage.getId()
      Languages.update(newLanguage.getId(),{
        $set:{
            name: newLanguage.getName(),
            shortNames: newLanguage.getShortNames(),
            image: newLanguage.getImage()
            isDefault: newLanguage.getIsDefault()
        }
      });
    else
      Languages.insert({
        name: newLanguage.getName(),
        shortNames: newLanguage.getShortNames(),
        image: newLanguage.getImage(),
        isDefault: newLanguage.getIsDefault()
      });

  delete: (language) ->
    Languages.remove({_id:language.getId()});