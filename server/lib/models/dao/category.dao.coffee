class @CategoryDAO

  constructor:() ->

  exists:(newCategory) ->
    category = Categories.find({ name: newCategory.getName() }).fetch()

    if newCategory.getId()
      if category.length == 1 and newCategory.getId() == category[0]._id
        false
      else
        true
    else
      if category and category.length > 0
        true
      else
        false

  save: (newCategory) ->
    if newCategory.getId()
      Categories.update(newCategory.getId(),{
        $set:{
          name: newCategory.getName(),
          color: newCategory.getColor()
        }
      });
    else
      Categories.insert({
        name: newCategory.getName(),
        color: newCategory.getColor()
      });

  delete: (category) ->
    Categories.remove({_id:category.getId()});
