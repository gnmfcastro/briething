class @Media

  @mediaId
  @name
  @type
  @url
  @urlSmall
  @urlThumb
  @from

  constructor:(@mediaId,@name,@type,@url,@urlSmall,@urlThumb,@from) ->

  getMediaId: () ->
    @mediaId
  setId: (@mediaId) ->
    @

  getName: () ->
    @name
  setName: (@name) ->
    @

  getType: () ->
    @type
  setType: (@type) ->
    @

  getUrl: () ->
    @url
  setUrl: (@url) ->
    @

  getUrlSmall: () ->
    @urlSmall
  serUrlSmall: (@urlSmall) ->
    @

  getUrlThumb: () ->
    @urlThumb
  serUrlSmall: (@urlThumb) ->
    @

  getFrom: () ->
    @from
  setFrom: (@from) ->
    @