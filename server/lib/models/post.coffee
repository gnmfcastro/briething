class @Post
  @id
  @title
  @subtitle
  @content
  @footer
  @publisher
  @language
  @categories

  constructor: (@title,@subtitle,@content,@footer,@publisher,@language,@categories) ->

  getId: () ->
    @id
  setId: (@id) ->
    @

  getTitle: () ->
    @title
  setTitle: (@title) ->
    @

  getSubtitle: () ->
    @subtitle
  setSubtitle: (@subtitle) ->
    @

  getContent: () ->
    @content
  setContent: (@content) ->
    @

  getFooter: () ->
    @footer
  setFooter: (@footer) ->
    @

  getPublisher: () ->
    @publisher
  setPublisher: (@publisher) ->
    @

  getLanguage: () ->
    @language
  setLanguage: (@language) ->
    @

  getCategories: () ->
    @categories
  setCategories: (@categories) ->
    @
