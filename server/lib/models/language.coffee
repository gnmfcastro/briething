class @Language
  @id
  @name
  @shortNames
  @image
  @isDefault

  constructor: (@name,@shortNames,@image,@isDefault) ->

  getId:() ->
    @id
  setId:(@id) ->
    @

  getName: () ->
    @name
  setName: (@name) ->
    @

  getShortNames: () ->
    @shortNames
  setShortNames: (@shortNames) ->
    @

  getImage:() ->
    @image
  setImage: (@image) ->
    @

  getIsDefault: () ->
    @isDefault
  setIdDefault: (@isDefault) ->
    @