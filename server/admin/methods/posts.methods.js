Meteor.methods({

    post_post: function(posts){

        var dao = new PostDAO();
        var daoPostLanguage = new PostLanguageDAO();
        var varPostLanguage = {
            posts:[],
            dateInserted: "",
            medias: [],
            categories: []
        };

        for(var i =0; i<posts.posts.length; i++) {
            var newPost = new Post(
                posts.posts[i].title,
                posts.posts[i].subtitle,
                posts.posts[i].content,
                posts.posts[i].footer,
                Meteor.userId(),
                posts.posts[i].language
            );

            varPostLanguage.posts.push(dao.save(newPost));
        }

        varPostLanguage.dateInserted = posts.dateInserted;

        for(var j=0; j<posts.medias.length; j++){
            var newMedia = new Media(
                posts.medias[j].mediaId,
                posts.medias[j].name,
                posts.medias[j].type,
                posts.medias[j].url,
                posts.medias[j].urlSmall,
                posts.medias[j].urlThumb,
                posts.medias[j].from
            );

            varPostLanguage.medias.push(newMedia);
        }

        for(var k=0; k<posts.categories.length; k++){
            var newCategory = new Category(
              posts.categories[k].name,
              posts.categories[k].color
            );

            newCategory.setId(posts.categories[k]._id);

            varPostLanguage.categories.push(newCategory);
        }

        var newPostLanguage = new PostLanguage(
            varPostLanguage.posts,
            varPostLanguage.medias,
            varPostLanguage.dateInserted,
            varPostLanguage.categories
        );

        daoPostLanguage.save(newPostLanguage);
    },

    post_update: function(posts){

        var dao = new PostDAO();
        var daoPostLanguage = new PostLanguageDAO();
        var varPostLanguage = {
            _id: "",
            posts:[],
            dateInserted: "",
            medias: [],
            categories: []
        };

        for(var i =0; i<posts.posts.length; i++) {
            var newPost = new Post(
                posts.posts[i].title,
                posts.posts[i].subtitle,
                posts.posts[i].content,
                posts.posts[i].footer,
                Meteor.userId(),
                posts.posts[i].language
            );

            newPost.setId(posts.posts[i]._id);

            if(dao.save(newPost) == 1) {
                varPostLanguage.posts.push(newPost.getId());
            }
        }

        varPostLanguage.dateInserted = posts.dateInserted;
        varPostLanguage._id = posts._id;

        for(var j=0; j<posts.medias.length; j++){
            var newMedia = new Media(
                posts.medias[j].mediaId,
                posts.medias[j].name,
                posts.medias[j].type,
                posts.medias[j].url,
                posts.medias[j].urlSmall,
                posts.medias[j].urlThumb,
                posts.medias[j].from
            );

            varPostLanguage.medias.push(newMedia);
        }

        for(var k=0; k<posts.categories.length; k++){
            var newCategory = new Category(
                posts.categories[k].name,
                posts.categories[k].color
            );

            newCategory.setId(posts.categories[k]._id);

            varPostLanguage.categories.push(newCategory);
        }

        var newPostLanguage = new PostLanguage(
            varPostLanguage.posts,
            varPostLanguage.medias,
            varPostLanguage.dateInserted,
            varPostLanguage.categories
        );

        newPostLanguage.setId(varPostLanguage._id);

        daoPostLanguage.save(newPostLanguage);
    }

});
