'use strict';
Meteor.methods({
    language_post: function(name,shortNames, image, isDefault){
        var dao = new LanguageDAO();
        var newLanguage = new Language(name, shortNames, image, isDefault);

        if(dao.exists(newLanguage)){
            throw new Meteor.Error(400,"Language already registered.");
        }else{
            if(dao.existsDefault(newLanguage)){
                throw new Meteor.Error(401, "There is another language that is default");
            }else{
                dao.save(newLanguage);
            }
        }
    },

    language_update: function(language){
        var dao = new LanguageDAO();
        var languageToUpdate = new Language(language.name,language.shortNames, language.image, language.isDefault);
        languageToUpdate.setId(language._id);

        if(dao.exists(languageToUpdate)){
            throw new Meteor.Error(400,"Language already registered.");
        }else{
            if(dao.existsDefault(languageToUpdate)){
                throw new Meteor.Error(401, "There is another language that is default");
            }else{
                dao.save(languageToUpdate);
            }
        }
    },

    language_delete: function(language){
        var dao = new LanguageDAO();
        var languageToDelete = new Language(language.name,language.shortNames, language.image);
        languageToDelete.setId(language._id);

        dao.delete(languageToDelete);
    }
});
