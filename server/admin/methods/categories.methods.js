/**
 * Created by guilherme on 9/15/15.
 */
'use strict';
Meteor.methods({

    category_post : function(name, color){
        var dao = new CategoryDAO();
        var newCategory = new Category(name, color);

        if(dao.exists(newCategory)){
            throw new Meteor.Error(400,"Category already registered.");
        }else{
            dao.save(newCategory);
        }
    },

    category_update : function(category){
        var dao = new CategoryDAO();
        var categoryToUpdate = new Category(category.name,category.color);
        categoryToUpdate.setId(category._id);

        if(dao.exists(categoryToUpdate)){
            throw new Meteor.Error(400,"Category already registered.");
        }else{
            if(dao.existsDefault(categoryToUpdate)){
                throw new Meteor.Error(401, "There is another language that is default");
            }else{
                dao.save(categoryToUpdate);
            }
        }
    },

    category_delete: function(category){
        var dao = new CategoryDAO();
        var categoryToDelete = new Language(category.name,category.color);
        categoryToDelete.setId(category._id);

        dao.delete(categoryToDelete);
    }

});
