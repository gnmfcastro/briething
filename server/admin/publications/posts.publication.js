Meteor.publish('posts',function(){
   return Posts.find({});
});

Meteor.publish('postsByLanguage',function(languageId){
   return Posts.find({ language: languageId});
});
